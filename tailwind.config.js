/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundColor: {
        "btn-primary": {
          default: "#4945C6",
          hover: "#2C28A9",
          pressed: "#181495",
          disabled: "#D9DBDD",
        },
        "btn-secondary": {
          default: "#FFF",
          hover: "#FFF",
          pressed: "#FFF",
          disabled: "#FFF",
        },
        "btn-ghost-1": {
          default: "#FFF",
          hover: "#F4F4F4",
          pressed: "#F0F0F0",
          disabled: "#FFF",
        },
        "btn-ghost-2": {
          default: "#FFF",
          hover: "#F3F5FD",
          pressed: "#EFF1F9",
          disabled: "#FFF",
        },
      },
      padding: {
        11.5: "11.5px",
        12: "12px",
      },
      textColor: {
        "btn-text-secondary": "#464F60",
        "btn-text-ghost-1": "#464F60",
        "btn-text-ghost-2": "#4945C6",
        "btn-disabled": "#A5A7AB",
      },
      width: {
        156: "156px",
      },
      colors: {
        "custom-blue": "#4945C6",
      },
      borderColor: {
        active: "rgba(73, 69, 198, 0.60)",
      },
    },
  },
  plugins: [],
};
