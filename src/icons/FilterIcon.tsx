export const FilterIcon: React.FC<{ color: string }> = ({ color }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={20}
      height={20}
      viewBox="0 0 20 20"
      fill="none"
    >
      <path
        d="M16.0404 3.95801H3.95703L7.75848 8.70984C7.9949 9.00534 8.1237 9.37251 8.1237 9.75101V15.208C8.1237 15.6683 8.49678 16.0413 8.95703 16.0413H11.0404C11.5006 16.0413 11.8737 15.6683 11.8737 15.208V9.75101C11.8737 9.37251 12.0025 9.00534 12.2389 8.70984L16.0404 3.95801Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
