export const ColumnsIcon: React.FC<{ color: string }> = ({ color }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={20}
      height={20}
      viewBox="0 0 20 20"
      fill="none"
    >
      <path
        d="M4.79036 16.0415H15.207C15.6673 16.0415 16.0404 15.6684 16.0404 15.2082V4.79134C16.0404 4.33111 15.6673 3.95801 15.207 3.95801H4.79036C4.33013 3.95801 3.95703 4.33111 3.95703 4.79134V15.2082C3.95703 15.6684 4.33013 16.0415 4.79036 16.0415Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.70703 4.16699V15.8337"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.293 4.16699V15.8337"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
