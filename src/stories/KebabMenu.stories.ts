import { Meta, StoryObj } from "@storybook/react";
import { KebabMenu } from "../components/KebabMenu";
const meta: Meta<typeof KebabMenu> = {
  component: KebabMenu,
  title: "Kebab Menu",
  tags: ["autodocs"],
  argTypes: {
    isActive: {
      control: "boolean",
    },
  },
};

export default meta;

type Story = StoryObj<typeof meta>;

export const Kebab: Story = {
  args: {
    isActive: false,
  },
};
