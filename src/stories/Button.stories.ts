import { Meta, StoryObj } from "@storybook/react";
import { Button } from "../components/Button";
const meta: Meta<typeof Button> = {
  component: Button,
  title: "Button",
  tags: ["autodocs"],
  argTypes: {
    variants: {
      control: { type: "select" },
      options: ["primary", "secondary", "ghost-1", "ghost-2"],
    },
    size: {
      control: { type: "select" },
      options: ["normal", "compact", "small"],
    },
    disabled: {
      control: "boolean",
    },
    Icon: { control: false },
  },
};

export default meta;

type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {
    variants: "primary",
    size: "normal",
    children: "Button",
    disabled: false,
  },
};

export const Secondary: Story = {
  args: {
    variants: "secondary",
    size: "normal",
    children: "Button",
    disabled: false,
  },
};

export const GhostType1: Story = {
  args: {
    variants: "ghost-1",
    size: "normal",
    children: "Button",
    disabled: false,
  },
};

export const GhostType2: Story = {
  args: {
    variants: "ghost-2",
    size: "normal",
    children: "Button",
    disabled: false,
  },
};
