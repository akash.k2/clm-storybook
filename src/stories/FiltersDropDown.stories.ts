import { Meta, StoryObj } from "@storybook/react";
import { FilterDropDown } from "../components/FiltersDropDown";
const meta: Meta<typeof FilterDropDown> = {
  component: FilterDropDown,
  title: "Filters Dropdown",
  tags: ["autodocs"],
  argTypes: {
    isActive: {
      control: "boolean",
    },
    text: {
      control: "text",
    },
  },
};

export default meta;

type Story = StoryObj<typeof meta>;

export const Filters: Story = {
  args: {
    isActive: false,
  },
};
