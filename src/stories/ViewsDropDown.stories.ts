import { Meta, StoryObj } from "@storybook/react";
import { ViewsDropDown } from "../components/ViewsDropDown";
const meta: Meta<typeof ViewsDropDown> = {
  component: ViewsDropDown,
  title: "Views Dropdown",
  tags: ["autodocs"],
  argTypes: {
    isActive: {
      control: "boolean",
    },
    text: {
      control: "text",
    },
  },
};

export default meta;

type Story = StoryObj<typeof meta>;

export const Views: Story = {
  args: {
    isActive: false,
  },
};
