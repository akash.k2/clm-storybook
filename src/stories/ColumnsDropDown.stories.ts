import { Meta, StoryObj } from "@storybook/react";
import { ColumnsDropDown } from "../components/ColumnsDropDown";
const meta: Meta<typeof ColumnsDropDown> = {
  component: ColumnsDropDown,
  title: "Columns Dropdown",
  tags: ["autodocs"],
  argTypes: {
    isActive: {
      control: "boolean",
    },
    text: {
      control: "text",
    },
  },
};

export default meta;

type Story = StoryObj<typeof meta>;

export const Filters: Story = {
  args: {
    isActive: false,
  },
};
