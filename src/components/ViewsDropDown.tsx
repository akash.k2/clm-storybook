import { ChevronDownIcon } from "../icons/ChevronDownIcon";
import { ViewsIcon } from "../icons/ViewsIcon";

export interface Props {
  isActive: boolean;
  text: string;
}

export const ViewsDropDown = ({
  isActive,
  text = "Views",
  ...props
}: Props) => {
  const getColor = () => {
    if (isActive === true) {
      return "#4945C6";
    } else return "#464F60";
  };

  const getTextStyle = () => {
    let style = "text-base font-medium leading-normal";
    if (isActive === true) {
      return style + " text-custom-blue";
    } else return style + " text-gray-700";
  };

  const getActiveBorderStyles = () => {
    if (isActive === true) {
      return "border-solid border-active hover:border-active active:border-active";
    } else
      return "border-gray-300 hover:border-solid hover:border-gray-400 active:border-gray-500 ";
  };

  return (
    <div
      className={`flex cursor-pointer h-9 w-156 flex-shrink-0 justify-between items-center rounded-md py-11.5 pr-2 pl-3 bg-white border ${getActiveBorderStyles()}`}
      {...props}
    >
      <div className="flex items-center gap-1">
        <ViewsIcon color={getColor()} />
        <p className={getTextStyle()}>{text}</p>
      </div>
      <ChevronDownIcon color={getColor()} />
    </div>
  );
};
