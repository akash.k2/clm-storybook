import { KebabIcon } from "../icons/KebabIcon";

export interface Props {
  isActive: boolean;
}

export const KebabMenu = ({ isActive, ...props }: Props) => {
  const getColor = () => {
    if (isActive === true) {
      return "#4945C6";
    } else return "#464F60";
  };

  const getActiveStateStyles = () => {
    if (isActive === true) {
      return "border-solid border-active hover:border-active active:border-active";
    } else
      return "border-gray-300 hover:border-solid hover:border-gray-400 active:border-gray-500";
  };

  return (
    <div
      className={`inline-flex cursor-pointer h-9 flex-shrink-0 gap-1 justify-center items-center rounded-md px-1 py-11.5 bg-white border ${getActiveStateStyles()}`}
      {...props}
    >
      <KebabIcon color={getColor()} />
    </div>
  );
};
