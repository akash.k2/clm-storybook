import { ColumnsIcon } from "../icons/ColumnsIcon";

export interface Props {
  isActive: boolean;
  text: string;
}

export const ColumnsDropDown = ({
  isActive,
  text = "Columns",
  ...props
}: Props) => {
  const getColor = () => {
    if (isActive === true) {
      return "#4945C6";
    } else return "#464F60";
  };

  const getTextStyle = () => {
    let style = "text-base font-medium leading-normal";
    if (isActive === true) {
      return style + " text-custom-blue";
    } else return style + " text-gray-700";
  };

  const getActiveBorderStyles = () => {
    if (isActive === true) {
      return "border border-solid border-active hover:border-active active:border-active";
    } else
      return "border border-gray-300 hover:border-solid hover:border-gray-400 active:border-gray-500";
  };

  return (
    <div
      className={`inline-flex cursor-pointer h-9 gap-1 flex-shrink-0 justify-center items-center rounded-md py-2 px-3 bg-white ${getActiveBorderStyles()}`}
      {...props}
    >
      <ColumnsIcon color={getColor()} />
      <p className={getTextStyle()}>{text}</p>
    </div>
  );
};
