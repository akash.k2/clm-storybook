import { ReactNode } from "react";

export interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  children: ReactNode;
  variants: "primary" | "secondary" | "ghost-1" | "ghost-2";
  size: "normal" | "compact" | "small";
  disabled?: boolean;
  Icon?: React.FC<{ color: string }>;
  iconPosition?: "left" | "right";
}

export const Button = ({
  children,
  variants,
  disabled = false,
  size = "normal",
  Icon,
  iconPosition,
  ...props
}: Props) => {
  const getVariantStyles = () => {
    switch (variants) {
      case "primary":
        return "bg-btn-primary-default text-white";
      case "secondary":
        return "bg-btn-secondary-default text-btn-text-secondary border border-solid border-gray-300";
      case "ghost-1":
        return "bg-btn-ghost-1-default text-btn-text-ghost-1";
      case "ghost-2":
        return "bg-btn-ghost-2-default text-btn-text-ghost-2";
    }
  };

  const getHoverStyles = () => {
    switch (variants) {
      case "primary":
        return "hover:bg-btn-primary-hover hover:text-white";
      case "secondary":
        return "hover:border hover:border-solid hover:border-gray-400";
      case "ghost-1":
        return "hover:bg-btn-ghost-1-hover";
      case "ghost-2":
        return "hover:bg-btn-ghost-2-hover";
    }
  };

  const getColor = () => {
    if (disabled) {
      return "#A5A7AB";
    } else if (variants === "primary") {
      return "white";
    } else if (variants === "ghost-2") {
      return "#4945C6";
    } else {
      return "#464F60";
    }
  };

  const getHeight = () => {
    switch (size) {
      case "normal":
        return "h-9";
      case "compact":
        return "h-8";
      case "small":
        return "h-7";
    }
  };

  const getActiveStyles = () => {
    switch (variants) {
      case "primary":
        return "active:bg-btn-primary-pressed active:text-white";
      case "secondary":
        return "active:border hover:border-solid active:border-gray-600";
      case "ghost-1":
        return "active:bg-btn-ghost-1-pressed";
      case "ghost-2":
        return "active:bg-btn-ghost-2-pressed";
    }
  };

  const disabledStyles = () => {
    if (disabled) {
      let style = "disabled:text-btn-disabled disabled:cursor-not-allowed";
      if (variants === "primary") {
        return style + " disabled:bg-btn-primary-disabled ";
      } else if (variants === "secondary") {
        return style + " disabled:border-gray-300";
      } else return style + " disabled:bg-white";
    } else return "";
  };

  return (
    <button
      type="button"
      className={`inline-flex flex-shrink-0 gap-1 justify-center items-center rounded-md px-12 py-11.5 text-base font-medium ${getHeight()} ${getVariantStyles()} ${getHoverStyles()} ${getActiveStyles()} ${disabledStyles()}`}
      disabled={disabled}
      {...props}
    >
      {Icon && iconPosition === "left" && <Icon color={getColor()} />}
      {children}
      {Icon && iconPosition === "right" && <Icon color={getColor()} />}
    </button>
  );
};
